package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Utilisateur;

/**
 * Classe d'acces aux donnees contenues dans la table Utilisateur
 * 
 * @author ESIGELEC - Mariella& Darryl
 * @version 1.0
 */
public class UtilisateurDAO extends ConnexionDAO {

	private String prenom = null;
	private String id_Utilisateur = null;
	private String filiere = null;
	private String sexe = null;
	private String mail = null;
	private String telephon = null;
	private String motdp = null;

	public String getMotdp() {
		return motdp;
	}

	public void setMotdp(String motdp) {
		this.motdp = motdp;
	}

	private String nomprenom = null;

	public String getNomprenom() {
		return nomprenom;
	}

	public void setNomprenom(String nomprenom) {
		this.nomprenom = nomprenom;
	}

	private String nom = null;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getId_Utilisateur() {
		return id_Utilisateur;
	}

	public void setId_Utilisateur(String id_Utilisateur) {
		this.id_Utilisateur = id_Utilisateur;
	}

	public String getFiliere() {
		return filiere;
	}

	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephon() {
		return telephon;
	}

	public void setTelephon(String telephon) {
		this.telephon = telephon;
	}

	/**
	 * Constructor
	 * 
	 */
	public UtilisateurDAO() {
		super();

	}

	/**
	 * Permet d'ajouter un utilisateur dans la table utilisateur. Le mode est
	 * auto-commit par defaut : chaque insertion est validee
	 * 
	 * @param utilisateur l'utilisateur � ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Utilisateur utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement(
					"INSERT INTO utilisateur(id_utilisateur, nom, prenom, email, filiere,sexe, telephone, mot_de_passe,id_role) VALUES(?, ?, ?, ?,?,?,?,?,?)");
			ps.setInt(1, utilisateur.getId_utilisateur());
			ps.setString(2, utilisateur.getNom());
			ps.setString(3, utilisateur.getPrenom());
			ps.setString(4, utilisateur.getEmail());
			ps.setString(5, utilisateur.getFiliere());
			ps.setString(6, utilisateur.getSexe());
			ps.setString(7, utilisateur.getTelephone());
			ps.setString(8, utilisateur.getMot_de_passe());
			ps.setString(9, utilisateur.getId_role());
			// Execution de la requete
			returnValue = ps.executeUpdate();
			JOptionPane.showMessageDialog(null, "Etudiant Ajouté!");

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de fournisseur existe déjà. Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de modifier un utilisateur dans la table supplier. Le mode est
	 * auto-commit par defaut : chaque modification est validee
	 * 
	 * @param Utilisateur l'utilisateur a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	public ResultSet afficher() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("Select * from cours");
			rs = ps.executeQuery();
			// ps = con.prepareStatement(
			// "UPDATE utilisateur set nom= ?, prenom= ?, email= ?, filiere= ?,sexe= ?,
			// telephone=?,id_role=?, WHERE id_utilisateur = ?");
			/*
			 * ps.setInt(1, utilisateur.getId_utilisateur()); ps.setString(2,
			 * utilisateur.getNom()); ps.setString(3, utilisateur.getPrenom());
			 * ps.setString(4, utilisateur.getEmail()); ps.setString(5,
			 * utilisateur.getFiliere()); ps.setString(6, utilisateur.getSexe());
			 * ps.setString(7, utilisateur.getTelephone()); ps.setInt(8,
			 * utilisateur.getid_Role());
			 */

			// Execution de la requete
			// returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} /*
			 * finally { // fermeture du preparedStatement et de la connexion try { if (ps
			 * != null) { ps.close(); } } catch (Exception ignore) { } try { if (con !=
			 * null) { con.close(); } } catch (Exception ignore) { } }
			 */
		return rs;
	}

	/**
	 * Permet de supprimer un Utilisateur par id_utilisateur dans la table
	 * utilisateur. Si ce dernier a des cours , la suppression n'a pas lieu. Le mode
	 * est auto-commit par defaut : chaque suppression est validee
	 * 
	 * @param id l'id de l'utilisateur à supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int delete(int id_utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID du fournisseur
			ps = con.prepareStatement("DELETE FROM utilisateur WHERE id_utilisateur = ?");
			ps.setInt(1, id_utilisateur);

			// Execution de la requete
			returnValue = ps.executeUpdate();

			JOptionPane.showMessageDialog(null, "Etudiant Supprimé !");

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-02292")) {
				System.out.println("  Suppression impossible !");
			} else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de recuperer un utilisateur a partir de sa reference
	 * 
	 * @param reference la reference de l'utilisateur a recuperer
	 * @return l'utilisateur trouve; null si aucun utilisateur ne correspond a cette
	 *         reference
	 */
	public Utilisateur get(int id_utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Utilisateur returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM supplier WHERE id_utilisateur  = ?");
			ps.setInt(1, id_utilisateur);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Utilisateur(rs.getInt("id_utilisateur"), rs.getString("nom"), rs.getString("prenom"),
						rs.getString("email"), rs.getString("filiere"), rs.getString("sexe"), rs.getString("telephone"),
						rs.getString("id_role"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de recuperer tous les utilisateurs stockes dans la table utilisateur
	 * 
	 * @return une ArrayList d'utilisateurs
	 */
	/*
	 * public ArrayList<Utilisateur> getList() { Connection con = null;
	 * PreparedStatement ps = null; ResultSet rs = null; ArrayList<Utilisateur>
	 * returnValue = new ArrayList<Utilisateur>();
	 * 
	 * // connexion a la base de donnees try { con =
	 * DriverManager.getConnection(URL, LOGIN, PASS); ps =
	 * con.prepareStatement("SELECT * FROM utilisateur ORDER BY id_utilisateur");
	 * 
	 * // on execute la requete rs = ps.executeQuery(); // on parcourt les lignes du
	 * resultat while (rs.next()) { returnValue.add(new
	 * Utilisateur(rs.getInt("id_utilisateur"), rs.getString("name"),
	 * rs.getInt("id_utilisateur"), rs.getString("nom"), rs.getString("prenom"),
	 * rs.getString("email"), rs.getString("filiere"), rs.getString("sexe"),
	 * rs.getString("telephone"), rs.getInt("id_role")));
	 * 
	 * }
	 * 
	 * } catch (Exception ee) { ee.printStackTrace(); } finally { // fermeture du
	 * rs, du preparedStatement et de la connexion try { if (rs != null) rs.close();
	 * } catch (Exception ignore) { } try { if (ps != null) ps.close(); } catch
	 * (Exception ignore) { } try { if (con != null) con.close(); } catch (Exception
	 * ignore) { } } return returnValue; }
	 */

	public String Verification(String id, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> list = new ArrayList<>();

		String r = " ";
		String a = "0";
		// connexion a la base de donnees
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM utilisateur ORDER BY id_utilisateur");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next()) {
				if ((id.equals(rs.getString("id_utilisateur"))) && (password.equals(rs.getString("mot_de_passe")))) {

					r = "1";
					nomprenom = rs.getString("nom") + " " + rs.getString("prenom");

				}

			}
			if (r != "1") {
				JOptionPane.showMessageDialog(null, "Utilisateur Introuvable!");
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		if (r.equals("1"))
			return r;
		if (r.equals(null))
			r = a;
		return r;
	}

	public Object EtudRecherche(String id) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> list = new ArrayList<>();

		String r = " ";
		String a = "0";
		// connexion a la base de donnees
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM utilisateur ORDER BY id_utilisateur");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next()) {
				if ((id.equals(rs.getString("id_utilisateur")))) {

					r = "1";
					nomprenom = rs.getString("nom") + " " + rs.getString("prenom");
					nom = rs.getString("nom");
					prenom = rs.getString("prenom");
					id_Utilisateur = rs.getString("id_utilisateur");
					filiere = rs.getString("filiere");
					sexe = rs.getString("sexe");
					mail = rs.getString("email");
					telephon = rs.getString("telephone");
					motdp = rs.getString("mot_de_passe");
				}

			}
			if (r != "1") {
				JOptionPane.showMessageDialog(null, "Etudiant Introuvable!");
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		if (r.equals("1"))
			return r;
		if (r.equals(null))
			r = a;
		return r;
	}

	public int update(Utilisateur utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement(
					"UPDATE utilisateur set nom= ?, prenom= ?, email= ?, filiere= ?,sexe= ?, telephone=?,id_role=?, WHERE id_utilisateur = ?");
			ps.setInt(1, utilisateur.getId_utilisateur());
			ps.setString(2, utilisateur.getNom());
			ps.setString(3, utilisateur.getPrenom());
			ps.setString(4, utilisateur.getEmail());
			ps.setString(5, utilisateur.getFiliere());
			ps.setString(6, utilisateur.getSexe());
			ps.setString(7, utilisateur.getTelephone());
			ps.setInt(8, utilisateur.getid_Role());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * ATTENTION : Cette méthode n'a pas vocation à être executée lors d'une
	 * utilisation normale du programme ! Elle existe uniquement pour TESTER les
	 * méthodes écrites au-dessus !
	 * 
	 * @param args non utilisés
	 * @throws SQLException si une erreur se produit lors de la communication avec
	 *                      la BDD
	 */
	/*
	 * public static void main(String[] args) throws SQLException { int returnValue;
	 * SupplierDAO supplierDAO = new SupplierDAO(); // Ce test va utiliser
	 * directement votre BDD, on essaie d'éviter les collisions avec vos données en
	 * prenant de grands ID int[] ids = {424242, 424243, 424244}; // test du
	 * constructeur Supplier s1 = new Supplier(ids[0], "Mon fournisseur principal",
	 * "Rouen", "monfournisseurprincipal@mail.com"); Supplier s2 = new
	 * Supplier(ids[1], "Mon fournisseur secondaire", "Le Havre",
	 * "monfournisseursecondaire@mail.com"); Supplier s3 = new Supplier(ids[2],
	 * "Mon fournisseur de secours", "Paris", "monfournisseursecours@mail.com"); //
	 * test de la methode add returnValue = supplierDAO.add(s1);
	 * System.out.println(returnValue + " fournisseur ajoute"); returnValue =
	 * supplierDAO.add(s2); System.out.println(returnValue + " fournisseur ajoute");
	 * returnValue = supplierDAO.add(s3); System.out.println(returnValue +
	 * " fournisseur ajoute"); System.out.println();
	 * 
	 * // test de la methode get Supplier sg = supplierDAO.get(1); // appel
	 * implicite de la methode toString de la classe Object (a eviter)
	 * System.out.println(sg); System.out.println();
	 * 
	 * // test de la methode getList ArrayList<Supplier> list =
	 * supplierDAO.getList(); for (Supplier s : list) { // appel explicite de la
	 * methode toString de la classe Object (a privilegier)
	 * System.out.println(s.toString()); } System.out.println(); // test de la
	 * methode delete // On supprime les 3 articles qu'on a créé returnValue = 0;
	 * for (int id : ids) { // returnValue = supplierDAO.delete(id);
	 * System.out.println(returnValue + " fournisseur supprime"); }
	 * 
	 * System.out.println(); }
	 */

}

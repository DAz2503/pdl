package dao;

/**
 * Classe d'acces a la base de donnees
 * 
 * @author ESIGELEC - Mariella& Darryl
 * @version 1.0
 */
public class ConnexionDAO {
	/**
	 * Parametres de connexion a la base de donnees oracle URL, LOGIN et PASS sont
	 * des constantes
	 */
	// � utiliser si vous �tes sur une machine personnelle :
	final static String URL = "jdbc:oracle:thin:@//oracle.esigelec.fr:1521/orcl.intranet.int";

	// � utiliser si vous �tes sur une machine de l'�cole :
	// final static String URL =
	// "jdbc:oracle:thin:@//srvoracledb.intranet.int:1521/orcl.intranet.int";

	final static String LOGIN = "C##BDD4_11"; // remplacer les ********. Exemple C##BDD1_1
	final static String PASS = "BDD411"; // remplacer les ********. Exemple BDD11

	/**
	 * Constructor
	 * 
	 */
	public ConnexionDAO() {
		// chargement du pilote de bases de donnees
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println(
					"Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
}

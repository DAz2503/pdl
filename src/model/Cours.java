package model;

/**
 * Classe Utilisateur
 * 
 * @author ESIGELEC - Mariella& Darryl
 * @version 1.0
 */
public class Cours {
	/**
	 * reference de l'utilisateur
	 */
	private int id_utilisateur;
	/**
	 * nom
	 */
	private String nom;
	/**
	 * prenom
	 */
	private String prenom;
	/**
	 * email
	 */
	private String email;
	/**
	 * fili�re
	 */
	private String filiere;
	/**
	 * sexe
	 */
	private String sexe;
	/**
	 * num�ro de t�l�phone
	 */
	private String telephone;

	/**
	 * Mot de passe
	 */
	private String mot_de_passe;

	/**
	 * Constructor
	 * 
	 * @param id_utilisateur identifiant de l'utiisateur;
	 * @param nom            nom de l'utilisateur
	 * @param prenom         de l'utilisateur
	 * @param email          adresse mail du contact
	 * @param fili�re        la fili�re des �tudiants
	 * @param sexe           f�minin ou masculin
	 * @param telephone      num�ro de t�l�phone
	 * 
	 */
	public Cours(int id_utilisateur, String nom, String prenom, String email, String filiere, String sexe,
			String telephone, String mot_de_passe) {
		this.id_utilisateur = id_utilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.filiere = filiere;
		this.sexe = sexe;
		this.telephone = telephone;
		this.mot_de_passe = mot_de_passe;

	}

	/**
	 * Constructor
	 * 
	 * @param id_utilisateur identifiant de l'utiisateur;
	 * @param nom            nom de l'utilisateur
	 * @param prenom         de l'utilisateur
	 * @param email          adresse mail du contact
	 * @param sexe           f�minin ou masculin
	 * @param telephone      num�ro de t�l�phone
	 * 
	 */
	public Cours(int id_utilisateur, String nom, String prenom, String email, String sexe, String telephone,
			String mot_de_passe) {
		this.id_utilisateur = id_utilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.sexe = sexe;
		this.telephone = telephone;
		this.mot_de_passe = mot_de_passe;

	}

	public Cours(int int1, String string, String string2, String string3, String string4, String string5,
			String string6, int int2) {
		// TODO Auto-generated constructor stub
	}

	public Cours(int int1, String string, int int2, String string2, String string3, String string4, String string5,
			String string6, String string7, int int3) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id_utilisateur
	 */
	public int getId_utilisateur() {
		return id_utilisateur;
	}

	/**
	 * @param id_utilisateur the id_utilisateur to set
	 */
	public void setId_utilisateur(int id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the filiere
	 */
	public String getFiliere() {
		return filiere;
	}

	/**
	 * @param filiere the filiere to set
	 */
	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}

	/**
	 * @return the sexe
	 */
	public String getSexe() {
		return sexe;
	}

	/**
	 * @param sexe the sexe to set
	 */
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Redefinition de la methode toString permettant de definir la traduction de
	 * l'objet en String pour l'affichage dans la console par exemple
	 */
	@Override
	public String toString() {
		return "Utilisateur[ref : " + id_utilisateur + ", " + nom + ", " + prenom + ", " + email + filiere + ", "
				+ telephone + "," + mot_de_passe + "]";
	}

	public int getid_Role() {
		// TODO Auto-generated method stub
		return 0;
	}

}
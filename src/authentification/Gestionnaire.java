package authentification;

public class Gestionnaire {
	private int idG;
	private String nomG;

	public Gestionnaire(int idG, String nomG) {
		super();
		this.idG = idG;
		this.nomG = nomG;
	}

	public int getIdG() {
		return idG;
	}

	public void setIdG(int idG) {
		this.idG = idG;
	}

	public String getNomG() {
		return nomG;
	}

	public void setNomG(String nomG) {
		this.nomG = nomG;
	}

}

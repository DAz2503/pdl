package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class MenuGestionnaire {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public MenuGestionnaire(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(Color.RED));
		panel.setBounds(10, 22, 836, 61);
		frmEsigApp.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNom = new JLabel("");

		lblNom.setForeground(Color.RED);
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblNom.setBounds(164, 16, 378, 35);

		lblNom.setText(etud.getNom());
		panel.add(lblNom);

		JLabel lblBienvenue = new JLabel("Bienvenue ");
		lblBienvenue.setBackground(Color.WHITE);
		lblBienvenue.setBounds(10, 10, 780, 45);
		panel.add(lblBienvenue);
		lblBienvenue.setForeground(Color.RED);
		lblBienvenue.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblBienvenue.setHorizontalAlignment(SwingConstants.LEFT);

		JButton btnEtudiant = new JButton("ETUDIANT");
		btnEtudiant.setBackground(Color.RED);
		btnEtudiant.setForeground(Color.WHITE);
		btnEtudiant.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				Gestionnaire_Etudiant window = new Gestionnaire_Etudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEtudiant.setBounds(117, 123, 277, 61);
		frmEsigApp.getContentPane().add(btnEtudiant);

		JButton btnPROF = new JButton("PROFESSEUR");
		btnPROF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				Gestionnaire_Professeur window = new Gestionnaire_Professeur(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnPROF.setBackground(Color.RED);
		btnPROF.setForeground(Color.WHITE);
		btnPROF.setFont(new Font("Tahoma", Font.BOLD, 17));

		btnPROF.setBounds(117, 194, 277, 61);
		frmEsigApp.getContentPane().add(btnPROF);

		JButton btnJADMIN = new JButton("ADMINISTRATEUR");
		btnJADMIN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				Gestionnaire_Administrateur window = new Gestionnaire_Administrateur(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnJADMIN.setBackground(Color.RED);
		btnJADMIN.setForeground(Color.WHITE);
		btnJADMIN.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnJADMIN.setBounds(457, 123, 277, 61);
		frmEsigApp.getContentPane().add(btnJADMIN);

		JButton btnSeDeconnecter = new JButton("Se déconnecter");
		btnSeDeconnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frmLoginSystem = new JFrame("Se déconnecter");
				if (JOptionPane.showConfirmDialog(frmLoginSystem, "Confirm if you want to exit", "Login Systems",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					// System.exit(0);
					Log window = new Log();
					frmEsigApp.setVisible(false);
				}

			}
		});

		btnSeDeconnecter.setForeground(Color.WHITE);
		btnSeDeconnecter.setBackground(Color.RED);
		btnSeDeconnecter.setBounds(688, 460, 128, 29);
		frmEsigApp.getContentPane().add(btnSeDeconnecter);

		JButton btnGESTIONNAIRE = new JButton("GESTIONNNAIRE");
		btnGESTIONNAIRE.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnGESTIONNAIRE.setForeground(Color.WHITE);
		btnGESTIONNAIRE.setBackground(Color.RED);
		btnGESTIONNAIRE.setBounds(457, 194, 277, 61);
		frmEsigApp.getContentPane().add(btnGESTIONNAIRE);

		JButton btnCours = new JButton("COURS");
		btnCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				Gestionnaire_Cours window = new Gestionnaire_Cours(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnCours.setBackground(Color.RED);
		btnCours.setForeground(Color.WHITE);
		btnCours.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnCours.setBounds(283, 290, 277, 61);
		frmEsigApp.getContentPane().add(btnCours);
	}
}

package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.UtilisateurDAO;
import model.Utilisateur;

public class SupprimerEtudiant {

	private JFrame frmEsigApp;
	private JTextField NomEtudfield;
	private JButton btnConsulter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public SupprimerEtudiant(Utilisateur etudi) {
		initialize(etudi);
		frmEsigApp.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Utilisateur etudi) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				RechercherEtudiant window = new RechercherEtudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);

		JLabel lblPrenom = new JLabel("Prénom :");
		lblPrenom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPrenom.setBounds(111, 209, 69, 25);
		frmEsigApp.getContentPane().add(lblPrenom);

		JLabel lblMail = new JLabel("E-mail :");
		lblMail.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMail.setBounds(111, 244, 57, 23);
		frmEsigApp.getContentPane().add(lblMail);

		JLabel lblSEXE = new JLabel("Sexe :");
		lblSEXE.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSEXE.setBounds(470, 215, 57, 13);
		frmEsigApp.getContentPane().add(lblSEXE);

		JLabel groupE = new JLabel("");
		groupE.setBounds(246, 356, 168, 25);
		frmEsigApp.getContentPane().add(groupE);

		JPanel panel = new JPanel();
		panel.setBounds(64, 161, 680, 160);
		frmEsigApp.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel filE = new JLabel("");
		filE.setBounds(135, 119, 104, 20);
		panel.add(filE);
		filE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		filE.setText(etudi.getFiliere());

		JLabel lblTel = new JLabel("Téléphone :");
		lblTel.setBounds(47, 118, 91, 23);
		panel.add(lblTel);
		lblTel.setFont(new Font("Tahoma", Font.BOLD, 14));

		JLabel mailE = new JLabel("");
		mailE.setBounds(135, 82, 153, 20);
		panel.add(mailE);
		mailE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		mailE.setText(etudi.getEmail());

		JLabel penomE = new JLabel("");
		penomE.setBounds(135, 51, 129, 21);
		panel.add(penomE);
		penomE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		penomE.setText(etudi.getPrenom());

		// UtilisateurDAO etudiant = new UtilisateurDAO();
		// etudiant.get(Integer.parseInt());

		JLabel lblNomE = new JLabel("");
		lblNomE.setBounds(135, 21, 168, 20);
		panel.add(lblNomE);
		lblNomE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomE.setText(etudi.getNom());

		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(47, 20, 69, 23);
		panel.add(lblNom);
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 14));

		JLabel lblID = new JLabel("ID :");
		lblID.setBounds(404, 81, 45, 23);
		panel.add(lblID);
		lblID.setFont(new Font("Tahoma", Font.BOLD, 14));

		JLabel mdE = new JLabel("");
		mdE.setBounds(516, 119, 136, 20);
		panel.add(mdE);
		mdE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		mdE.setText(etudi.getMot_de_passe());

		JLabel lblMdp = new JLabel("Mot de passe :");
		lblMdp.setBounds(404, 118, 110, 23);
		panel.add(lblMdp);
		lblMdp.setFont(new Font("Tahoma", Font.BOLD, 14));

		JLabel idE = new JLabel("");
		idE.setBounds(516, 82, 297, 20);
		panel.add(idE);
		idE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		idE.setText(String.valueOf(etudi.getId_utilisateur()));

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(516, 50, 297, 23);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setText(etudi.getSexe());

		JLabel telE = new JLabel("");
		telE.setBounds(516, 21, 297, 20);
		panel.add(telE);
		telE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		telE.setText(etudi.getTelephone());

		JLabel lblFiliere = new JLabel("Filière :");
		lblFiliere.setBounds(404, 20, 57, 23);
		panel.add(lblFiliere);
		lblFiliere.setFont(new Font("Tahoma", Font.BOLD, 14));

		JLabel lblNomE1 = new JLabel("Rechercher un étudiant id");
		lblNomE1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNomE1.setBounds(113, 83, 195, 23);
		frmEsigApp.getContentPane().add(lblNomE1);

		NomEtudfield = new JTextField(String.valueOf(etudi.getId_utilisateur()));
		NomEtudfield.setBounds(313, 87, 374, 19);
		frmEsigApp.getContentPane().add(NomEtudfield);
		NomEtudfield.setColumns(10);

		JButton btnEnter = new JButton("Entrer");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String etudRe = NomEtudfield.getText();
				UtilisateurDAO etudi = new UtilisateurDAO();

				if (etudRe.length() > 0) {

					if (etudi.EtudRecherche(NomEtudfield.getText()).equals("1")) {

						// UtilisateurDAO e1 = new UtilisateurDAO();
						// Etudiant e1 = new Etudiant(1, "Darryl");

						Utilisateur a1 = new Utilisateur(Integer.parseInt(etudi.getId_Utilisateur()), etudi.getNom(),
								etudi.getPrenom(), etudi.getMail(), etudi.getFiliere(), etudi.getSexe(),
								etudi.getTelephon(), etudi.getMotdp(), "ggh");
						SupprimerEtudiant window = new SupprimerEtudiant(a1);
						frmEsigApp.setVisible(false);

						// etud.get(Integer.parseInt(etudRe));

					}

				}

			}
		});
		btnEnter.setForeground(Color.WHITE);
		btnEnter.setBackground(Color.RED);
		btnEnter.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnEnter.setBounds(426, 116, 85, 21);
		frmEsigApp.getContentPane().add(btnEnter);

		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String etudRe = NomEtudfield.getText();
				UtilisateurDAO etudi = new UtilisateurDAO();

				if (etudRe.length() > 0) {

					if (etudi.EtudRecherche(NomEtudfield.getText()).equals("1")) {

						// UtilisateurDAO e1 = new UtilisateurDAO();
						Etudiant e1 = new Etudiant(1, "Darryl");

						Utilisateur a1 = new Utilisateur(Integer.parseInt(etudi.getId_Utilisateur()), etudi.getNom(),
								etudi.getPrenom(), etudi.getMail(), etudi.getFiliere(), etudi.getSexe(),
								etudi.getTelephon(), etudi.getMotdp(), "ggh");
						etudi.delete(Integer.parseInt(etudRe));

						PageApresSupp window = new PageApresSupp(a1);

						frmEsigApp.setVisible(false);

						// etud.get(Integer.parseInt(etudRe));

					}

				}

			}
		});
		btnNewButton.setBackground(Color.RED);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(744, 461, 85, 35);
		frmEsigApp.getContentPane().add(btnNewButton);

	}
}

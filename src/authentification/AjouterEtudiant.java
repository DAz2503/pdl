package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.UtilisateurDAO;
import model.Utilisateur;

public class AjouterEtudiant {

	private JFrame frmEsigApp;
	private JTextField Namefield;
	private JTextField prenomf;
	private JTextField mailf;
	private JTextField telf;
	private JTextField idf;
	private JTextField mdf;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public AjouterEtudiant(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				Gestionnaire_Etudiant window = new Gestionnaire_Etudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);

		JLabel lblNom = new JLabel("Nom");
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNom.setBounds(132, 84, 69, 23);
		frmEsigApp.getContentPane().add(lblNom);

		Namefield = new JTextField();
		Namefield.setBounds(246, 88, 297, 19);
		frmEsigApp.getContentPane().add(Namefield);
		Namefield.setColumns(10);

		JLabel lblPrenom = new JLabel("Prénom");
		lblPrenom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPrenom.setBounds(132, 112, 57, 25);
		frmEsigApp.getContentPane().add(lblPrenom);

		JLabel lblMail = new JLabel("E-mail");
		lblMail.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMail.setBounds(132, 142, 57, 23);
		frmEsigApp.getContentPane().add(lblMail);

		prenomf = new JTextField();
		prenomf.setBounds(246, 117, 297, 19);
		frmEsigApp.getContentPane().add(prenomf);
		prenomf.setColumns(10);

		mailf = new JTextField();
		mailf.setBounds(246, 146, 297, 19);
		frmEsigApp.getContentPane().add(mailf);
		mailf.setColumns(10);

		JLabel lblFiliere = new JLabel("Filière");
		lblFiliere.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFiliere.setBounds(132, 202, 57, 23);
		frmEsigApp.getContentPane().add(lblFiliere);
		Object[] elements = new Object[] { "FISE", "FISA" };
		JComboBox<String> fil = new JComboBox(elements);
		fil.setBackground(Color.WHITE);
		fil.setBounds(246, 204, 200, 23);
		frmEsigApp.getContentPane().add(fil);

		JLabel lblSEXE = new JLabel("Sexe");
		lblSEXE.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSEXE.setBounds(132, 240, 57, 13);
		frmEsigApp.getContentPane().add(lblSEXE);
		Object[] sexe = new Object[] { "Masculin", "Féminin" };
		JComboBox<String> sex = new JComboBox(sexe);
		sex.setBackground(Color.WHITE);
		sex.setBounds(246, 237, 200, 23);
		frmEsigApp.getContentPane().add(sex);

		JLabel lblidrole = new JLabel("Id Role");
		lblidrole.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblidrole.setBounds(132, 329, 75, 23);
		frmEsigApp.getContentPane().add(lblidrole);
		Object[] role = new Object[] { "1 (Gestionnaire)", "2 (Administrateur)", "3 (Professeur)", "4 (Etudiant)" };
		JComboBox<String> rol = new JComboBox(role);
		rol.setBackground(Color.WHITE);
		rol.setBounds(246, 330, 270, 22);
		frmEsigApp.getContentPane().add(rol);

		JLabel lblTel = new JLabel("Téléphone");
		lblTel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTel.setBounds(132, 171, 83, 23);
		frmEsigApp.getContentPane().add(lblTel);

		telf = new JTextField();
		telf.setBounds(246, 175, 297, 19);
		frmEsigApp.getContentPane().add(telf);
		telf.setColumns(10);

		JLabel lblID = new JLabel("ID");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblID.setBounds(132, 263, 45, 23);
		frmEsigApp.getContentPane().add(lblID);

		idf = new JTextField();
		idf.setBounds(246, 270, 297, 19);
		frmEsigApp.getContentPane().add(idf);
		idf.setColumns(10);

		JButton btnADD = new JButton("ADD");
		btnADD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String Nom = Namefield.getText();
				String prenom = prenomf.getText();
				String mail = mailf.getText();
				String tel = telf.getText();
				String id = idf.getText();
				String sexe = null;
				String mdp = mdf.getText();
				String filiere = null;
				String role = null;
				if (fil.getSelectedItem().equals("FISE")) {
					filiere = "FISE";
				}

				else if (fil.getSelectedItem().equals("FISA")) {
					filiere = "FISA";
				}
				if (sex.getSelectedItem().equals("Masculin")) {
					sexe = "Masculin";
				}

				else if (sex.getSelectedItem().equals("Féminin")) {
					sexe = "Féminin";
				}
				if (rol.getSelectedItem().equals("1 (Gestionnaire)")) {
					role = "1 (Gestionnaire)";
				}

				else if (rol.getSelectedItem().equals("2 (Administrateur)")) {
					role = "2 (Administrateur)";
				}

				else if (rol.getSelectedItem().equals("3 (Professeur)")) {
					role = "3 (Professeur)";
				}

				else if (rol.getSelectedItem().equals("4 (Etudiant)")) {
					role = "4 (Etudiant)";
				}

				Utilisateur etudiant = new Utilisateur(Integer.parseInt(id), Nom, prenom, mail, tel, sexe, filiere, mdp,
						role);
				UtilisateurDAO a = new UtilisateurDAO();
				a.add(etudiant);
				Etudiant e1 = new Etudiant(1, "Darryl");
				AjouterEtudiant window = new AjouterEtudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnADD.setBackground(Color.RED);
		btnADD.setForeground(Color.WHITE);
		btnADD.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnADD.setBounds(743, 462, 91, 31);
		frmEsigApp.getContentPane().add(btnADD);

		JLabel lblMdp = new JLabel("Mot de passe");
		lblMdp.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMdp.setBounds(132, 295, 110, 23);
		frmEsigApp.getContentPane().add(lblMdp);

		mdf = new JTextField();
		mdf.setBounds(246, 299, 297, 19);
		frmEsigApp.getContentPane().add(mdf);
		mdf.setColumns(10);

	}
}

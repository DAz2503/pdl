package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Gestionnaire_Etudiant {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public Gestionnaire_Etudiant(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnAjouterEtudiant = new JButton("Ajouter Etudiant");
		btnAjouterEtudiant.setBackground(Color.RED);
		btnAjouterEtudiant.setForeground(Color.WHITE);
		btnAjouterEtudiant.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAjouterEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				AjouterEtudiant window = new AjouterEtudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnAjouterEtudiant.setBounds(299, 107, 277, 61);
		frmEsigApp.getContentPane().add(btnAjouterEtudiant);

		JButton btnSupprimerEtudiant = new JButton("Supprimer Etudiant");
		btnSupprimerEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				recherche_etud_supp window = new recherche_etud_supp(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnSupprimerEtudiant.setBackground(Color.RED);
		btnSupprimerEtudiant.setForeground(Color.WHITE);
		btnSupprimerEtudiant.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnSupprimerEtudiant.setBounds(299, 194, 277, 61);
		frmEsigApp.getContentPane().add(btnSupprimerEtudiant);

		JButton btnJModifierEtudiant = new JButton("Modifier Etudiant");
		btnJModifierEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnJModifierEtudiant.setBackground(Color.RED);
		btnJModifierEtudiant.setForeground(Color.WHITE);
		btnJModifierEtudiant.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnJModifierEtudiant.setBounds(299, 285, 277, 61);
		frmEsigApp.getContentPane().add(btnJModifierEtudiant);

		JButton btnConsulterEtudiant = new JButton("Consulter Etudiant");
		btnConsulterEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Etudiant e1 = new Etudiant(1, "Darryl");
				RechercherEtudiant window = new RechercherEtudiant(e1);
				frmEsigApp.setVisible(false);

			}
		});
		btnConsulterEtudiant.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnConsulterEtudiant.setForeground(Color.WHITE);
		btnConsulterEtudiant.setBackground(Color.RED);
		btnConsulterEtudiant.setBounds(299, 389, 277, 61);
		frmEsigApp.getContentPane().add(btnConsulterEtudiant);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				MenuGestionnaire window = new MenuGestionnaire(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);
	}
}

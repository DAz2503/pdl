package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Gestionnaire_Administrateur {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public Gestionnaire_Administrateur(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnAjouterAdmin = new JButton("Ajouter Administrateur");
		btnAjouterAdmin.setBackground(Color.RED);
		btnAjouterAdmin.setForeground(Color.WHITE);
		btnAjouterAdmin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAjouterAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAjouterAdmin.setBounds(299, 107, 277, 61);
		frmEsigApp.getContentPane().add(btnAjouterAdmin);

		JButton btnSupprimerAdmin = new JButton("Supprimer Administrateur");
		btnSupprimerAdmin.setBackground(Color.RED);
		btnSupprimerAdmin.setForeground(Color.WHITE);
		btnSupprimerAdmin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnSupprimerAdmin.setBounds(299, 194, 277, 61);
		frmEsigApp.getContentPane().add(btnSupprimerAdmin);

		JButton btnJModifierAdmin = new JButton("Modifier Administrateur");
		btnJModifierAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnJModifierAdmin.setBackground(Color.RED);
		btnJModifierAdmin.setForeground(Color.WHITE);
		btnJModifierAdmin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnJModifierAdmin.setBounds(299, 285, 277, 61);
		frmEsigApp.getContentPane().add(btnJModifierAdmin);

		JButton btnConsulterAdmin = new JButton("Consulter Administrateur");
		btnConsulterAdmin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnConsulterAdmin.setForeground(Color.WHITE);
		btnConsulterAdmin.setBackground(Color.RED);
		btnConsulterAdmin.setBounds(299, 389, 277, 61);
		frmEsigApp.getContentPane().add(btnConsulterAdmin);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				MenuGestionnaire window = new MenuGestionnaire(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);
	}
}

package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.UtilisateurDAO;
import model.Utilisateur;

public class PageApresSupp {

	private JFrame frmEsigApp;
	private JTextField NomEtudfield;
	private JButton btnConsulter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public PageApresSupp(Utilisateur etudi) {
		initialize(etudi);
		frmEsigApp.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Utilisateur etudi) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				RechercherEtudiant window = new RechercherEtudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);

		JLabel groupE = new JLabel("");
		groupE.setBounds(246, 356, 168, 25);
		frmEsigApp.getContentPane().add(groupE);

		JLabel lblNomE1 = new JLabel("Rechercher un étudiant id");
		lblNomE1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNomE1.setBounds(113, 83, 195, 23);
		frmEsigApp.getContentPane().add(lblNomE1);

		NomEtudfield = new JTextField();
		NomEtudfield.setBounds(313, 87, 374, 19);
		frmEsigApp.getContentPane().add(NomEtudfield);
		NomEtudfield.setColumns(10);

		JButton btnEnter = new JButton("Entrer");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String etudRe = NomEtudfield.getText();
				UtilisateurDAO etudi = new UtilisateurDAO();

				if (etudRe.length() > 0) {

					if (etudi.EtudRecherche(NomEtudfield.getText()).equals("1")) {

						// UtilisateurDAO e1 = new UtilisateurDAO();
						// Etudiant e1 = new Etudiant(1, "Darryl");

						Utilisateur a1 = new Utilisateur(Integer.parseInt(etudi.getId_Utilisateur()), etudi.getNom(),
								etudi.getPrenom(), etudi.getMail(), etudi.getFiliere(), etudi.getSexe(),
								etudi.getTelephon(), etudi.getMotdp(), "ggh");
						SupprimerEtudiant window = new SupprimerEtudiant(a1);
						frmEsigApp.setVisible(false);

						// etud.get(Integer.parseInt(etudRe));

					}

				}

			}
		});
		btnEnter.setForeground(Color.WHITE);
		btnEnter.setBackground(Color.RED);
		btnEnter.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnEnter.setBounds(426, 116, 85, 21);
		frmEsigApp.getContentPane().add(btnEnter);

	}
}

package authentification;

public class Etudiant {
	private int idE;
	private String nom;

	public Etudiant(int idE, String nom) {
		super();
		this.idE = idE;
		this.nom = nom;
	}

	public int getIdE() {
		return idE;
	}

	public void setIdE(int idE) {
		this.idE = idE;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}

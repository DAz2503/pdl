package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class MenuEtudiant {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public MenuEtudiant(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(Color.RED));
		panel.setBounds(10, 22, 836, 61);
		frmEsigApp.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNom = new JLabel("");

		lblNom.setForeground(Color.RED);
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblNom.setBounds(164, 16, 378, 35);

		lblNom.setText(etud.getNom());
		panel.add(lblNom);

		JLabel lblBienvenue = new JLabel("Bienvenue ");
		lblBienvenue.setBackground(Color.WHITE);
		lblBienvenue.setBounds(10, 10, 780, 45);
		panel.add(lblBienvenue);
		lblBienvenue.setForeground(Color.RED);
		lblBienvenue.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblBienvenue.setHorizontalAlignment(SwingConstants.LEFT);

		JButton btnCPlanning = new JButton("CONSULTER PLANNING");
		btnCPlanning.setBackground(Color.RED);
		btnCPlanning.setForeground(Color.WHITE);
		btnCPlanning.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnCPlanning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnCPlanning.setBounds(299, 133, 277, 75);
		frmEsigApp.getContentPane().add(btnCPlanning);

		JButton btnCAbs = new JButton("CONSULTER VOS ABSENCES");
		btnCAbs.setBackground(Color.RED);
		btnCAbs.setForeground(Color.WHITE);
		btnCAbs.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnCAbs.setBounds(299, 262, 277, 75);
		frmEsigApp.getContentPane().add(btnCAbs);

		JButton btnJAbs = new JButton("JUSTIFIER VOS ABSENCES");
		btnJAbs.setBackground(Color.RED);
		btnJAbs.setForeground(Color.WHITE);
		btnJAbs.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnJAbs.setBounds(299, 381, 277, 75);
		frmEsigApp.getContentPane().add(btnJAbs);

		JButton btnSeDeconnecter = new JButton("Se déconnecter");
		btnSeDeconnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frmLoginSystem = new JFrame("Se déconnecter");
				if (JOptionPane.showConfirmDialog(frmLoginSystem, "Confirm if you want to exit", "Login Systems",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					// System.exit(0);
					Log window = new Log();
					frmEsigApp.setVisible(false);
				}

			}
		});

		btnSeDeconnecter.setForeground(Color.WHITE);
		btnSeDeconnecter.setBackground(Color.RED);
		btnSeDeconnecter.setBounds(688, 460, 128, 29);
		frmEsigApp.getContentPane().add(btnSeDeconnecter);
	}
}

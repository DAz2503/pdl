package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import model.Utilisateur;

public class EtudiantRecherche {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public EtudiantRecherche(Utilisateur etudi) {
		initialize(etudi);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Utilisateur etudi) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				RechercherEtudiant window = new RechercherEtudiant(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);

		JLabel lblNom = new JLabel("Nom :");
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNom.setBounds(132, 112, 69, 23);
		frmEsigApp.getContentPane().add(lblNom);

		JLabel lblPrenom = new JLabel("Prénom :");
		lblPrenom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPrenom.setBounds(132, 145, 69, 25);
		frmEsigApp.getContentPane().add(lblPrenom);

		JLabel lblMail = new JLabel("E-mail :");
		lblMail.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMail.setBounds(132, 180, 57, 23);
		frmEsigApp.getContentPane().add(lblMail);

		JLabel lblFiliere = new JLabel("Filière :");
		lblFiliere.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFiliere.setBounds(132, 237, 57, 23);
		frmEsigApp.getContentPane().add(lblFiliere);

		JLabel lblSEXE = new JLabel("Sexe :");
		lblSEXE.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSEXE.setBounds(132, 270, 57, 13);
		frmEsigApp.getContentPane().add(lblSEXE);

		JLabel lblTel = new JLabel("Téléphone :");
		lblTel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTel.setBounds(132, 208, 104, 23);
		frmEsigApp.getContentPane().add(lblTel);

		JLabel lblID = new JLabel("ID :");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblID.setBounds(132, 293, 45, 23);
		frmEsigApp.getContentPane().add(lblID);

		JLabel lblMdp = new JLabel("Mot de passe :");
		lblMdp.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMdp.setBounds(132, 326, 110, 23);

		frmEsigApp.getContentPane().add(lblMdp);

		// UtilisateurDAO etudiant = new UtilisateurDAO();
		// etudiant.get(Integer.parseInt());

		JLabel lblNomE = new JLabel("");
		lblNomE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomE.setBounds(246, 115, 297, 20);
		lblNomE.setText(etudi.getNom());
		frmEsigApp.getContentPane().add(lblNomE);

		JLabel penomE = new JLabel("");
		penomE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		penomE.setBounds(246, 149, 297, 21);
		penomE.setText(etudi.getPrenom());
		frmEsigApp.getContentPane().add(penomE);

		JLabel mailE = new JLabel("");
		mailE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		mailE.setBounds(246, 183, 297, 20);
		mailE.setText(etudi.getEmail());
		frmEsigApp.getContentPane().add(mailE);

		JLabel telE = new JLabel("");
		telE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		telE.setBounds(246, 238, 297, 20);
		telE.setText(etudi.getTelephone());
		frmEsigApp.getContentPane().add(telE);

		JLabel filE = new JLabel("");
		filE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		filE.setBounds(246, 209, 297, 20);
		filE.setText(etudi.getFiliere());
		frmEsigApp.getContentPane().add(filE);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(246, 265, 297, 23);
		lblNewLabel.setText(etudi.getSexe());
		frmEsigApp.getContentPane().add(lblNewLabel);

		JLabel idE = new JLabel("");
		idE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		idE.setBounds(246, 296, 297, 20);
		idE.setText(String.valueOf(etudi.getId_utilisateur()));
		frmEsigApp.getContentPane().add(idE);

		JLabel mdE = new JLabel("");
		mdE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		mdE.setBounds(246, 326, 273, 20);
		mdE.setText(etudi.getMot_de_passe());
		frmEsigApp.getContentPane().add(mdE);

		JLabel lblNewLabel_1 = new JLabel("Groupe :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(132, 361, 104, 23);
		frmEsigApp.getContentPane().add(lblNewLabel_1);

		JLabel groupE = new JLabel("");
		groupE.setBounds(246, 356, 168, 25);
		frmEsigApp.getContentPane().add(groupE);

	}
}

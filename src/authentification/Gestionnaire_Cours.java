package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Gestionnaire_Cours {

	private JFrame frmEsigApp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// MenuEtudiant window = new MenuEtudiant();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public Gestionnaire_Cours(Etudiant etud) {
		initialize(etud);
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frmEsigApp = new JFrame();
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 22));
		frmEsigApp.setResizable(false);
		frmEsigApp.setBounds(100, 100, 870, 563);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JButton btnAjouterCours = new JButton("Ajouter Cours");
		btnAjouterCours.setBackground(Color.RED);
		btnAjouterCours.setForeground(Color.WHITE);
		btnAjouterCours.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAjouterCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAjouterCours.setBounds(299, 107, 277, 61);
		frmEsigApp.getContentPane().add(btnAjouterCours);

		JButton btnSupprimerCours = new JButton("Supprimer Cours");
		btnSupprimerCours.setBackground(Color.RED);
		btnSupprimerCours.setForeground(Color.WHITE);
		btnSupprimerCours.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnSupprimerCours.setBounds(299, 194, 277, 61);
		frmEsigApp.getContentPane().add(btnSupprimerCours);

		JButton btnJModifierCours = new JButton("Modifier Cours");
		btnJModifierCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnJModifierCours.setBackground(Color.RED);
		btnJModifierCours.setForeground(Color.WHITE);
		btnJModifierCours.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnJModifierCours.setBounds(299, 285, 277, 61);
		frmEsigApp.getContentPane().add(btnJModifierCours);

		JButton btnConsulterCours = new JButton("Consulter Cours");
		btnConsulterCours.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnConsulterCours.setForeground(Color.WHITE);
		btnConsulterCours.setBackground(Color.RED);
		btnConsulterCours.setBounds(299, 389, 277, 61);
		frmEsigApp.getContentPane().add(btnConsulterCours);

		JButton btnEXIT = new JButton("Exit");
		btnEXIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Etudiant e1 = new Etudiant(1, "Darryl");
				MenuGestionnaire window = new MenuGestionnaire(e1);
				frmEsigApp.setVisible(false);
			}
		});
		btnEXIT.setBackground(Color.RED);
		btnEXIT.setForeground(Color.WHITE);
		btnEXIT.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnEXIT.setBounds(20, 460, 85, 35);
		frmEsigApp.getContentPane().add(btnEXIT);

		JLabel lblNameg = new JLabel("Nom : AZANDOSSESSI");
		lblNameg.setHorizontalAlignment(SwingConstants.LEFT);
		lblNameg.setBounds(20, 25, 195, 23);
		frmEsigApp.getContentPane().add(lblNameg);

		JLabel lblPrenomg = new JLabel("Prénom : Darryl");
		lblPrenomg.setBounds(20, 46, 110, 13);
		frmEsigApp.getContentPane().add(lblPrenomg);

		JLabel lblRole = new JLabel("Role: Gestionnaire");
		lblRole.setBounds(20, 61, 110, 13);
		frmEsigApp.getContentPane().add(lblRole);
	}
}

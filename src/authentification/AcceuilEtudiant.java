package authentification;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class AcceuilEtudiant {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// AcceuilEtudiant window = new AcceuilEtudiant();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public AcceuilEtudiant(Etudiant etud) {
		initialize(etud);
		frame.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Etudiant etud) {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Acceuil Etudiant");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(136, 10, 221, 40);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblNom = new JLabel("");
		lblNom.setBounds(169, 115, 45, 13);
		frame.getContentPane().add(lblNom);

		lblNom.setText(etud.getNom());
	}

}

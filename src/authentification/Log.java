package authentification;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import dao.UtilisateurDAO;

public class Log {

	private JFrame frmEsigApp;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Log window = new Log();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Log() {
		initialize();
		frmEsigApp.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEsigApp = new JFrame();
		frmEsigApp.getContentPane().setBackground(Color.WHITE);
		frmEsigApp.setTitle("ESIG APP");
		frmEsigApp.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\Logo app.png"));
		frmEsigApp.setBounds(200, 200, 563, 383);
		frmEsigApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsigApp.getContentPane().setLayout(null);

		JLabel lblUsername = new JLabel("Username or id");
		lblUsername.setBounds(39, 113, 98, 29);
		frmEsigApp.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(52, 173, 85, 13);
		frmEsigApp.getContentPane().add(lblPassword);

		JLabel lblNewLabel_2 = new JLabel("Login");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(227, 60, 104, 19);
		frmEsigApp.getContentPane().add(lblNewLabel_2);

		JLabel lblErreur = new JLabel("");
		lblErreur.setForeground(Color.RED);

		txtUsername = new JTextField();
		txtUsername.setBounds(142, 118, 240, 19);
		frmEsigApp.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(142, 170, 240, 19);
		frmEsigApp.getContentPane().add(txtPassword);

		JButton btnLogin = new JButton("Login");
		btnLogin.setForeground(Color.WHITE);
		btnLogin.setBackground(Color.RED);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Clicked");
				String username = txtUsername.getText();
				String password = txtPassword.getText();

				UtilisateurDAO etud = new UtilisateurDAO();

				if (username.length() > 0 && password.length() > 0) {
					lblErreur.setText("");
					if (password.contains("DAZ") && username.contains("darryl")) {
						Etudiant e1 = new Etudiant(1, "Darryl");
						MenuGestionnaire window = new MenuGestionnaire(e1);
						// AcceuilEtudiant window = new AcceuilEtudiant(e1);
						frmEsigApp.setVisible(false);
					} else if (etud.Verification(txtUsername.getText(), txtPassword.getText()).equals("1")) {

						Etudiant e2 = new Etudiant(1, etud.getNomprenom());
						MenuEtudiant window = new MenuEtudiant(e2);
						frmEsigApp.setVisible(false);
					} else {
						lblErreur.setText("Erreur : Login/password");
					}
				} else {
					lblErreur.setText("Erreur : Veuillez remplir les champs");
				}
				/*
				 * String password = txtPassword.getText(); String username =
				 * txtUsername.getText();
				 * 
				 * if (password.contains("DAZ") && username.contains("darryl")) {
				 * 
				 * txtPassword.setText(null); txtUsername.setText(null);
				 * 
				 * }
				 * 
				 * else { JOptionPane.showMessageDialog(null, "Invalid login details",
				 * "Login Error", JOptionPane.ERROR_MESSAGE);
				 * 
				 * txtPassword.setText(null); txtUsername.setText(null); }
				 */
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnLogin.setBounds(428, 281, 85, 21);
		frmEsigApp.getContentPane().add(btnLogin);

		JButton btnReset = new JButton("Reset");
		btnReset.setForeground(Color.WHITE);
		btnReset.setBackground(Color.RED);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				txtUsername.setText(null);
				txtPassword.setText(null);

			}
		});
		btnReset.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnReset.setBounds(39, 281, 85, 21);
		frmEsigApp.getContentPane().add(btnReset);

		JSeparator separator = new JSeparator();
		separator.setBounds(30, 248, 519, 7);
		frmEsigApp.getContentPane().add(separator);

		JLabel lblNewLabel = new JLabel("ESIGELEC");
		lblNewLabel.setIcon(null);
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 23));
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(0, 0, 124, 36);
		frmEsigApp.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\HP\\eclipse-workspace\\PageDeConnexion\\media\\minilogo.png"));
		lblNewLabel_1.setBounds(107, 0, 45, 36);
		frmEsigApp.getContentPane().add(lblNewLabel_1);

		JCheckBox chckbxNewCheckBox = new JCheckBox("Se souvenir de moi");
		chckbxNewCheckBox.setForeground(Color.BLUE);
		chckbxNewCheckBox.setFont(new Font("Times New Roman", Font.PLAIN, 10));
		chckbxNewCheckBox.setBackground(Color.WHITE);
		chckbxNewCheckBox.setBounds(142, 207, 112, 21);
		frmEsigApp.getContentPane().add(chckbxNewCheckBox);

		lblErreur.setBounds(142, 88, 240, 13);
		frmEsigApp.getContentPane().add(lblErreur);
	}
}
